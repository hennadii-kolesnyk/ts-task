<?php

require_once (__DIR__ . '/vendor/autoload.php');

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

define( 'ROOT_DIRECTORY', __DIR__);

try {
    $response = require_once (__DIR__ . '/app/kernel.php');
} catch (ResourceNotFoundException $exception) {
    $response = new Response('Not Found', Response::HTTP_NOT_FOUND);
} catch (Exception $exception) {
    $response = new Response('An error occurred', Response::HTTP_INTERNAL_SERVER_ERROR);
}

$response->send();

