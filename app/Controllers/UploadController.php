<?php

namespace App\Controllers;

use App\Services\FileUploaderService;
use League\Csv\Reader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UploadController
{
    /**
     * @var $request Request
     */
    private $request;

    /**
     * @var $uploader FileUploaderService
     */
    private $uploader;

    public function __construct()
    {
        $this->request = Request::createFromGlobals();
        $this->uploader = new FileUploaderService();
    }

    public function index()
    {
        return require_once (base_dir('/views/index.php'));
    }

    public function store()
    {
        $file = $this->request->files->get('file');

        if (empty($file)) {
            return new Response("No file specified", Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {

            if (!$this->isValidCSV($file)) {
                return new Response('Not supported file format',  Response::HTTP_UNPROCESSABLE_ENTITY,
                    ['content-type' => 'text/plain']);
            }

            $id = $this->uploader->upload($file);

            redirect("/stat/{$id}");
        } catch (\Exception $e) {
            return new Response($e->getMessage(),  Response::HTTP_INTERNAL_SERVER_ERROR,
                ['content-type' => 'text/plain']);
        }

    }

    /**
     * @param $file
     * @return bool
     *
     * Simple validation
     */
    private function isValidCSV($file)
    {
        if ($file->getClientMimeType() !== 'text/csv') {
            return false;
        }

        $stream = fopen($file->getPathname(), 'r');

        $csv = Reader::createFromStream($stream);

        $csv->setDelimiter(',');

        return (count($csv->fetchOne()) === 5);
    }
}