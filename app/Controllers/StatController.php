<?php


namespace App\Controllers;

use App\Models\CustomerStatistic;
use App\Services\IpAnalyzer\IpAnalyzer;
use App\Services\PhoneAnalyzer\PhoneAnalyzer;
use App\Services\CsvStatHandlerService;

class StatController
{
    /**
     * @var PhoneAnalyzer
     */
    public $phoneAnalyzer;

    /**
     * @var IpAnalyzer
     */
    public $ipAnalyzer;

    public function __construct()
    {
        $this->phoneAnalyzer = new PhoneAnalyzer();
        $this->ipAnalyzer    = new IpAnalyzer();
    }

    /**
     * This methods show customers statistics
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $statsGroupedByCustomer = CsvStatHandlerService::make($id)->present(500);

        foreach ($statsGroupedByCustomer as $customerId => $stats) {
            $statistics[] = CustomerStatistic::make($customerId)
                ->calculate($stats, $this->phoneAnalyzer, $this->ipAnalyzer);
        }

        return require_once (base_dir('/views/stat.php'));
    }

}