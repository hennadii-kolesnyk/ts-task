<?php

namespace App\Services\IpAnalyzer;

use GeoIp2\Database\Reader;

class GeoIpService extends AnalyzerHandler
{
    /**
     * @var $service Reader
     */
    private $service;

    public function __construct()
    {
        $this->service = new Reader(resource_file('GeoLite2-Country.mmdb'));
    }

    public function getContinentCode(string $ip): string
    {
        try {
            $record = $this->service->country($ip);

            return $record->continent->code;
        } catch (\Exception $e) {
            return $this->next($ip);
        }
    }
}