<?php


namespace App\Services\IpAnalyzer;

use App\Interfaces\ContinentIdentifier;
use App\Exceptions\UnprocessableContinentCodeException;

abstract class AnalyzerHandler implements ContinentIdentifier
{
    /**
     * @var $next self
     */
    protected $next;

    /**
     * @param AnalyzerHandler $next
     */
    public function push(self $next)
    {
        $this->next = $next;
    }

    /**
     * @param string $ip
     * @return string
     * @throws UnprocessableContinentCodeException
     */
    public function next(string $ip): string
    {
        if ($this->next) {
            return $this->next->getContinentCode($ip);
        }

        throw new UnprocessableContinentCodeException("Undefined Ip: {$ip}");
    }
}