<?php

namespace App\Services\IpAnalyzer;

use OK\Ipstack\Client;

class IpStackService extends AnalyzerHandler
{
    /**
     * @var $service Client
     */
    private $service;

    public function __construct()
    {
        $this->service = new Client('d9f000dbc0237078dfb39bf8033d244c');
    }

    public function getContinentCode(string $ip): string
    {
        try {
            $record = $this->service->get($ip);

            return $record->getContinentCode();
        } catch (\Exception $e) {
            return $this->next($ip);
        }
    }
}