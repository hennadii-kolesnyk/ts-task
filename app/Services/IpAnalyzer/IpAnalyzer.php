<?php


namespace App\Services\IpAnalyzer;

use App\Interfaces\ContinentIdentifier;

class IpAnalyzer implements ContinentIdentifier
{
    /**
     * @var IpStackService
     */
    private $service;

    public function __construct()
    {
        $this->service = new GeoIpService;

        $this->service->push(new IpStackService);
    }

    /**
     * @param string $ip
     * @return string
     */
    public function getContinentCode(string $ip): string
    {
        return $this->service->getContinentCode($ip);
    }
}