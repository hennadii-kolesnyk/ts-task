<?php

namespace App\Services\PhoneAnalyzer;

use App\Exceptions\UnprocessableContinentCodeException;
use Brick\PhoneNumber\PhoneNumber;

use App\Interfaces\ContinentIdentifier;

class PhoneAnalyzer implements ContinentIdentifier
{
    public function getContinentCode(string $phone): string
    {
        try {
            $phoneInfo = PhoneNumber::parse("+{$phone}");

            $countryCode = $phoneInfo->getRegionCode();

            if (NULL === $countryCode) {
                throw new UnprocessableContinentCodeException();
            }

            return get_continent_code_by_country_code($countryCode);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            throw new UnprocessableContinentCodeException();
        }
    }
}