<?php


namespace App\Services;

use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileUploaderService
{
    public function upload($file)
    {
        try {
            $fileName = $this->getFileName();
            $file->move(
                $this->getUploadDir(),
                $fileName
            );

            return $fileName;
        } catch (FileException $e){
            throw new FileException('Failed to upload file');
        }
    }

    private function getUploadDir()
    {
        return uploads_file('');
    }

    private function getFileName()
    {
        return time();
    }
}