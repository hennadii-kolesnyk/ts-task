<?php

namespace App\Services;

use League\Csv\Reader;
use League\Csv\Statement;

class CsvStatHandlerService
{
    private $csv;

    public static function make($uploadId)
    {
        $self = new self;
        $stream = fopen(uploads_file($uploadId), 'r');

        $self->csv = Reader::createFromStream($stream);

        $self->csv->setDelimiter(',');
        $self->csv->setHeaderOffset(0);

        return $self;
    }

    private function handle($limit)
    {
        $stmt = (new Statement())
            ->limit($limit);

        return $stmt->process($this->csv);
    }

    public function present($limit)
    {
        $records = $this->handle($limit);

        $statsGroupedByCustomers = [];
        foreach ($records as $record) {
            $statsGroupedByCustomers[array_shift($record)][] = array_combine(
                ['dateTime', 'duration', 'phone', 'ip'],
                array_values($record)
            );
        }

        return $statsGroupedByCustomers;
    }


}