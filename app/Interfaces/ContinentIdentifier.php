<?php

namespace App\Interfaces;

interface ContinentIdentifier
{
    public function getContinentCode(string $ip):string;
}