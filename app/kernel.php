<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

$mainRequestHandler = function (RouteCollection $routes) {
    $request = Request::createFromGlobals();
    $context = new RequestContext();
    $context->fromRequest($request);
    $matcher = new UrlMatcher($routes, $context);

    $controllerResolver = new ControllerResolver();
    $argumentResolver   = new ArgumentResolver();

    $request->attributes->add($matcher->match($request->getPathInfo()));

    $controller = $controllerResolver->getController($request);

    $arguments = $argumentResolver->getArguments($request, $controller);

    return call_user_func_array($controller, $arguments);
};

return $mainRequestHandler(require_once ('routes.php'));
