<?php

namespace App;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('index', new Route('/', [
    '_controller' => ['App\Controllers\UploadController', 'index']
]));

$collection->add('upload', new Route('/upload', [
    '_controller' => ['App\Controllers\UploadController', 'store']
]));

$collection->add('stat', new Route('/stat/{id}', [
    '_controller' => ['App\Controllers\StatController', 'show']
]));

return $collection;
