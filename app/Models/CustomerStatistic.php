<?php


namespace App\Models;

use App\Exceptions\UnprocessableContinentCodeException;
use App\Services\IpAnalyzer\IpAnalyzer;
use App\Services\PhoneAnalyzer\PhoneAnalyzer;

class CustomerStatistic
{
    private $customerId;
    private $numberSameContinentCalls = 0;
    private $durationSameContinentCalls = 0;
    private $numberTotalCalls = 0;
    private $durationTotalCalls = 0;

    public function __construct($customerId)
    {
        $this->customerId = $customerId;
    }

    public static function make($customerId)
    {
        return new self($customerId);
    }

    public function calculate(
        array $stats,
        PhoneAnalyzer $phoneAnalyzer,
        IpAnalyzer $ipAnalyzer
    )
    {
        foreach ($stats as $stat) {
            try {
                $this->durationTotalCalls+=$stat['duration'];
                $this->numberTotalCalls++;
                if ($ipAnalyzer->getContinentCode($stat['ip']) === $phoneAnalyzer->getContinentCode($stat['phone'])) {
                    $this->durationSameContinentCalls+=$stat['duration'];
                    $this->numberSameContinentCalls++;
                }
            } catch (UnprocessableContinentCodeException $e) {
                continue;
            }
        }

        return $this;
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }

    public function getNumberSameContinentCalls()
    {
        return $this->numberSameContinentCalls;
    }

    public function getDurationSameContinentCalls()
    {
        return $this->durationSameContinentCalls;
    }

    public function getNumberTotalCalls()
    {
        return $this->numberTotalCalls;
    }

    public function getDurationTotalCalls()
    {
        return $this->durationTotalCalls;
    }
}