<h1>Statistic for upload <?=$id?></h1>

<table>
    <tr>
        <th>Customer Id</th>
        <th>Number of customer's calls within same continent</th>
        <th>Total duration of customer's calls within same continent</th>
        <th>Number of all customer's calls</th>
        <th>Total duration of all customer's calls</th>
    </tr>
    <?php
    /**
     * @var $statistics array
     * @var $statistic \App\Models\CustomerStatistic
     */
    ?>
    <?php foreach ($statistics as $statistic):?>
    <tr>
        <td><?=$statistic->getCustomerId()?></td>
        <td><?=$statistic->getNumberSameContinentCalls()?></td>
        <td><?=$statistic->getDurationSameContinentCalls()?></td>
        <td><?=$statistic->getNumberTotalCalls()?></td>
        <td><?=$statistic->getDurationTotalCalls()?></td>
    </tr>
    <?php endforeach; ?>
</table>
